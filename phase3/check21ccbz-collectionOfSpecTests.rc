structdef HUMAN
{
	int age;
	bool single;
};

function : HUMAN human_val (HUMAN h)
{
	h.age = 29;
	h.single = true;
	return h;
}

function : HUMAN & human_ref (HUMAN & h)
{
	return h;
}

typedef int* BLAH;
int[5] arr1;
float[5] arr2;

function : void foo2 ( float * x ) {}

typedef int[5] ARR1;
typedef ARR1* PTR_ARR;
typedef float[5] F_ARR1;
typedef F_ARR1* PTR_ARR2;
BLAH s2;
PTR_ARR s22;
PTR_ARR2 s23;
function : ARR1* foo3 () { return s22;}
function : ARR1* & foo4() { return s23;}		// type of return expr (PTR_ARR2) is not equiv. to the
												// function's return type (ARR1*)
function : ARR1* foo5() { return s23;}			// type of return expr (PTR_ARR2) is not assign. to the
												// function's return type (ARR1*)

function : void foo() 
{
	BLAH x;
	bool y = x++;		// int* not assignable to bool
}

structdef MYS
{
	int x;
};

structdef MYS1
{
	int x;
};

MYS a;
MYS1 b;

function : int foo1(int a) { return 0; }

typedef funcptr : int (int b) MYFP;

function : void main () 
{
	HUMAN * h_ptr;
	HUMAN h;
	
	human_ref(h) = *h_ptr;
	human_val(h) = *h_ptr;			// left-hand operand is not a modifiable l-value

	*NULL;							// incompatible type NULL to unary dereference op *
	*(MYS *) &b = a;
	
	funcptr : float & (float ** x, int[6] & arr) fp8;	
	funcptr : float (float x, int[5] & arr) fp9 = fp8;	// not assignable
	
	MYFP MyFuncPtr;
	MyFuncPtr = foo1; 
	MyFuncPtr(1);
	
	MYFP * MyFuncPtrPtr;
	MyFuncPtrPtr = &foo1; 			// non-addressable arg of type funcptr : int (int a)
	MyFuncPtrPtr = &MyFuncPtr;
	(*MyFuncPtrPtr) (1); 
	
	MyFuncPtr = foo1;
	MyFuncPtr(5.5); 				// arg of ype float not assignable to value parameter b, of type int
	
	funcptr : int (int x) ptr1;
	ptr1 = foo1;
	
	funcptr : int (int x, int y) ptr2;
	ptr2 = foo1;					// not assignable
	
	foo2( arr2 );
	foo2( arr1 );					// arg of type int[5] not assignable to param x, of type float*
	
	int e = true++;					// incompatible type bool to operator ++
	const int f = 1++;				// operand to ++ is not a modifiable l-value
	const int i = 3;
	const int g = i++;				// operand to ++ is not a modifiable l-value
	int h = 5++;					// operand to ++ is not a modifiable l-value
	
	int y12;
	y12++;
	(y12++)++;						// operand to ++ is not a modifiable l-value
}

